#include <gtest/gtest.h>

#include "utility.h"

TEST(Checksum, XOR) {
	const char data[] = "a";

	unsigned char result = checksum((const unsigned char *) data, strlen(data));
	EXPECT_EQ(result, 0x61);
}

int main(int argc, char **argv)
{
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
