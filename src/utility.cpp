#include "utility.h"

unsigned char checksum(const void *data, unsigned short length)
{
	const unsigned char *bytes = static_cast<const unsigned char *>(data);
	unsigned char sum = 0;
	unsigned short i;

	for (i = 0; i < length; i++)
		sum ^= bytes[i];

	return sum;	
}
