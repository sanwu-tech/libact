#include "act.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include <libserialport.h>

#include "utility.h"

/* ATQA */
const unsigned char ATQA_MIFARE_ULTRALIGHT[] = { 0x00, 0x44 };
const unsigned char ATQA_MIFARE_S50_1K[]     = { 0x00, 0x04 };
const unsigned char ATQA_MIFARE_S70_4K[]     = { 0x00, 0x02 };

/* RDATA */
const unsigned char RDATA_SUCCESS[]          = { 0x90, 0x00 };
const unsigned char RDATA_FAILED[]           = { 0x6F, 0x00 };
const unsigned char RDATA_ADDRESS_OVERFLOW[] = { 0x6B, 0x00 };
const unsigned char RDATA_LENGTH_OVERFLOW[]  = { 0x67, 0x00 };

/* ICRW Config (S1 - S10) */
const unsigned char ICRW_CONFIG_S2_S4_CODE[] = { 'V', '1', '0' };

/* Internal Errors */
int E_ARG  = SP_ERR_ARG;
int E_FAIL = SP_ERR_FAIL;
int E_MEM  = SP_ERR_MEM;
int E_SUPP = SP_ERR_SUPP;
int E_IO   = -8;
int E_NULL = -9;

/* Custom Errors */
int E_INVALID_ADDRESS             = -10;
int E_INVALID_BAUDRATE            = -20;
int E_INVALID_PORT                = -30;
int E_COMMAND_NOT_SENT            = -40;
int E_RESPONSE_NOT_RECEIVED       = -50;
int E_DATA_LENGTH_TOO_HIGH        = -60;
int E_RESPONSE_CHECKSUM_MISMATCH  = -70;
int E_RESPONSE = -80;

#define MUST(f) \
	do { \
		if ((ret = (f)) < 0) \
			goto out; \
	} while (0)

struct act {
	struct sp_port *port;
	int address;
};

static unsigned int read_response_interval = DEFAULT_READ_RESPONSE_INTERVAL;
static unsigned char last_command = 0;

static void nsleep(time_t seconds, long nanoseconds)
{
	struct timespec t;

	t.tv_sec = seconds;
	t.tv_nsec = nanoseconds;

	nanosleep(&t, NULL);
}

static int is_response_data_complete(unsigned char *buf, unsigned short buf_len)
{
	if (buf_len < 11 || buf[0] != STX_VALUE || buf[buf_len - 2] != ETX_VALUE)
		return 0;

	if (checksum(buf, buf_len - 1) != buf[buf_len - 1])
		return E_RESPONSE_CHECKSUM_MISMATCH;

	return buf_len;
}

static int read_response_data(struct sp_port *port, struct response *resp, unsigned char *buf, unsigned short buf_cap)
{
	int buf_len = 0;
	int nwaiting = 0;
	int nread = 0;

	while (1) {
		nwaiting = sp_input_waiting(port);

		if (nwaiting > 0) {
			nread = sp_nonblocking_read(port, &buf[buf_len], buf_cap);
			buf_len += nread;

			if (is_response_data_complete(buf, buf_len) > 0)
				break;
			else if (nread < 0)
				goto out;

		} else if (nread == 0) {
			continue;
		} else {
			goto out;
		}

		nsleep(0, read_response_interval * 1000000);
	}

out:
	return nread;
}

#ifdef DEBUG
static void print_write(char *buf, int nwrite)
{
	char tmp[20] = { 0 };
	int i;

	sprintf(tmp, "write %d bytes: ", nwrite);

	fprintf(stdout, "%-20s", tmp);

	for (i = 0; i < nwrite; i++)
		fprintf(stdout, "%hhX ", buf[i]);

	fprintf(stdout, "\n");
}

static void print_read(char *buf, int nread)
{
	char tmp[20] = { 0 };
	int i;

	sprintf(tmp, "read %d bytes: ", nread);

	fprintf(stdout, "%-20s", tmp);

	for (i = 0; i < nread; i++)
		fprintf(stdout, "%hhX ", buf[i]);

	fprintf(stdout, "\n");
}
#endif

int act_connect(struct act **act, const char *name, int baudrate, int address)
{
	struct sp_port **ports = 0;
	struct sp_port *port = 0;
	size_t i = 0;
	int ret = 0;

	if (address > 0xF) {
		ret = E_INVALID_ADDRESS;
		goto out;
	}

	if (baudrate != 9600  &&
	    baudrate != 19200 &&
	    baudrate != 38400 &&
	    baudrate != 57600) {
		ret = E_INVALID_BAUDRATE;
		goto out;
	}

	if ((ret = sp_list_ports(&ports)) != 0)
		goto out;

	port = ports[i];

	while (port != NULL) {
		if (strcmp(name, sp_get_port_name(port)) == 0)
			break;

		port = ports[++i];
	}

	if (!port) {
		ret = E_INVALID_PORT;
		goto out;
	}

	*act = (struct act *) malloc(sizeof(**act));
	if (!(*act)) {
		ret = E_MEM;
		goto out;
	}

	(*act)->address = address;

	if ((ret = sp_copy_port(port, &(*act)->port)) != 0)
		goto out;

	if ((ret = sp_open((*act)->port, SP_MODE_READ_WRITE)) != 0)
		goto out;

	if ((ret = sp_set_baudrate((*act)->port, baudrate)) != 0)
		goto out;

	if ((ret = sp_set_bits((*act)->port, 8)) != 0)
		goto out;

	if ((ret = sp_set_parity((*act)->port, SP_PARITY_NONE)) != 0)
		goto out;

	if ((ret = sp_set_stopbits((*act)->port, 1)) != 0)
		goto out;

out:
	if (ports)
		sp_free_port_list(ports);

	if (ret < 0 && *act) {
		free(*act);
		*act = 0;
	}

	return ret;
}

int act_disconnect(struct act *act)
{
	int ret = 0;

	if (!(act && act->port)) {
		ret = E_NULL;
		goto out;
	}

	if ((ret = sp_close(act->port)) != 0)
		goto out;

	sp_free_port(act->port);

	free(act);

out:
	return ret;
}

int act_send(struct act *act, unsigned char command, unsigned char parameters, unsigned char *data, unsigned short data_length)
{
	const unsigned short length = data_length + 3;
	unsigned char buf[BUF_LEN];
	int ret;

	if (!(act && act->port)) {
		ret = E_NULL;
		goto out;
	}

	buf[STX]  = STX_VALUE;
	buf[ADDR] = act->address;
	buf[LENH] = length >> 8;
	buf[LENL] = length;
	buf[MT]   = CMT_VALUE;
	buf[CM]   = command;
	buf[PM]   = parameters;

	if (data) {
		if (data_length > DATA_LEN) {
			ret = E_DATA_LENGTH_TOO_HIGH;
			goto out;
		}

		memcpy(buf + 7, data, data_length);
	}

	buf[data_length + 7] = ETX_VALUE;
	buf[data_length + 8] = checksum(buf, data_length + 8);

	MUST(sp_nonblocking_write(act->port, buf, data_length + 9));
	MUST(sp_drain(act->port));

	last_command = command;

#ifdef DEBUG
	print_write((char *) buf, ret);
#endif

out:
	return ret;
}

int act_recv(struct act *act, struct response *r)
{
	unsigned char buf[BUF_LEN] = { 0 };
	unsigned short length;
	int ret;

	if (!(act && act->port)) {
		ret = E_NULL;
		goto out;
	}

	while (1) {
		ret = sp_input_waiting(act->port);

		if (ret > 0)
			break;
		else if (ret == 0)
			continue;
		else
			goto out;
	}

	MUST(sp_nonblocking_read(act->port, buf, 1));

#ifdef DEBUG
	print_read((char *) buf, ret);
#endif

	switch (buf[0]) {
	case ACK:
		break;

	default:
		goto out;
	}

	MUST(read_response_data(act->port, r, buf, BUF_LEN));

	if (last_command == RESET_CARD) {
		length = ret - 5;
		memmove(buf + 4, buf + 3, ret);
		buf[3] = length;
		ret += 1;
	} else {
		length = ((unsigned short) buf[2]) << 8 | buf[3];
	}

#ifdef DEBUG
	print_read((char *) buf, ret);
#endif

	if (buf[0] != STX_VALUE)
		goto out;

	r->address     = act->address;
	r->length      = length;
	r->code        = buf[4];
	r->command     = buf[5];
	r->parameters  = buf[6];

	if (buf[r->length + 5] == checksum(buf, r->length + 5)) {
		buf[0] = ACK;

		if ((ret = sp_blocking_write(act->port, buf, 1, 100)) <= 0)
			goto out;
	} else {
		buf[0] = NAK;

		if ((ret = sp_blocking_write(act->port, buf, 1, 100)) <= 0)
			goto out;

		ret = E_RESPONSE_CHECKSUM_MISMATCH;
		goto out;
	}

#ifdef DEBUG
	print_write((char *) buf, ret);
#endif

	if (r->code == RESPONSE_OK) {
		r->status.ok.st0 = buf[7];
		r->status.ok.st1 = buf[8];
		r->status.ok.st2 = buf[9];
		memcpy(r->data, buf + 10, r->length - 6);
	} else {
		r->status.error = ((unsigned short) buf[7]) << 8 | buf[8];
		memcpy(r->data, buf + 10, r->length - 5);
		ret = -1;
	}

out:
	return ret;
}

int act_initialize(struct act *act, struct response *r, int mode, char *output, int output_length)
{
	int ret;

	if (!(act && r)) {
		ret = E_NULL;
		goto out;
	}

	switch (mode) {
	case RESET_CARD_MOVE:
	case RESET_CARD_COLLECT:
	case RESET_CARD_MOVE_WITH_COUNT:
	case RESET_CARD_COLLECT_WITH_COUNT:
	case RESET_CARD_WITHOUT_MOVE:
	case RESET_CARD_WITHOUT_MOVE_WITH_COUNT:
		break;

	default:
		ret = E_ARG;
		goto out;
	}

	MUST(act_send(act, RESET_CARD, mode, 0, 0));
	MUST(act_recv(act, r));

	if (!output)
		goto out;

	if (output_length <= 0) {
		ret = E_ARG;
		goto out;
	}

	if (r->length < output_length)
		output_length = r->length;

	memcpy(output, r->data, output_length - 1);
	output[output_length - 1] = 0;

out:
	return ret;
}

int act_get_crs_status(struct act *act, struct response *r, struct crs_status *status)
{
	int ret;

	if (!(act && r)) {
		ret = E_NULL;
		goto out;
	}

	MUST(act_send(act, CHECK_STATUS, CHECK_STATUS_BASIC, 0, 0));
	MUST(act_recv(act, r));

	if (!status)
		goto out;

	status->lane         = r->status.ok.st0;
	status->card_box     = r->status.ok.st1;
	status->capture_full = r->status.ok.st2;

out:
	return ret;
}

int act_get_sensors_status(struct act *act, struct response * r, unsigned char (*sensors)[NUM_SENSORS])
{
	int ret;
	int i;

	if (!(act && r)) {
		ret = E_NULL;
		goto out;
	}

	MUST(act_send(act, CHECK_STATUS, CHECK_STATUS_BASIC_SE, 0, 0));
	MUST(act_recv(act, r));

	if (!(*sensors))
		goto out;

	for (i = 0; i < NUM_SENSORS; i++)
		(*sensors)[i] = r->data[i];

out:
	return ret;
}

int act_move_card(struct act *act, struct response *r, int mode)
{
	int ret = 0;

	if (!(act && r)) {
		ret = E_NULL;
		goto out;
	}

	switch (mode) {
	case MOVE_CARD_TO_EXIT:
	case MOVE_CARD_TO_COLLECTION:
	case MOVE_CARD_TO_OUTLET:
	case MOVE_CARD_TO_IC_SLOT:
	case MOVE_CARD_TO_RF_SLOT:
		break;

	default:
		goto out;
	}

	MUST(act_send(act, MOVE_CARD, mode, 0, 0));
	MUST(act_recv(act, r));

out:
	return ret;
}

int act_permit_insertion(struct act *act, struct response *r)
{
	int ret;

	if (!(act && r)) {
		ret = E_NULL;
		goto out;
	}

	MUST(act_send(act, SET_CARD, SET_CARD_PERMIT, 0, 0));
	MUST(act_recv(act, r));

out:
	return ret;
}

int act_deny_insertion(struct act *act, struct response *r)
{
	int ret;

	if (!(act && r)) {
		ret = E_NULL;
		goto out;
	}

	MUST(act_send(act, SET_CARD, SET_CARD_DENY, 0, 0));
	MUST(act_recv(act, r));

out:
	return ret;
}

int act_detect_ic(struct act *act, struct response *r, int *type)
{
	int ret;

	if (!(act && r)) {
		ret = E_NULL;
		goto out;
	}

	MUST(act_send(act, DETECT_CARD, DETECT_CARD_AUTO_IC, 0, 0));
	MUST(act_recv(act, r));

	if (!type)
		goto out;

	*type = *((unsigned short *) r->data);

out:
	return ret;
}

int act_detect_rf(struct act *act, struct response *r, int *type)
{
	int ret;

	if (!(act && r)) {
		ret = E_NULL;
		goto out;
	}

	MUST(act_send(act, DETECT_CARD, DETECT_CARD_AUTO_RF, 0, 0));
	MUST(act_recv(act, r));

	if (!type)
		goto out;

	*type = *((unsigned short *) r->data);

out:
	return ret;
}

int act_rf_activate(struct act *act, struct response *r, int first_protocol, int second_protocol, void *atr, int *atr_length)
{
	unsigned char data[2];
	int ret;

	if (!(act && r)) {
		ret = E_NULL;
		goto out;
	}

	if (first_protocol == second_protocol) {
		ret = E_ARG;
		goto out;
	}

	switch (first_protocol) {
	case '0':
	case 'A':
	case 'B':
		break;
	default:
		ret = E_ARG;
		goto out;
	}

	switch (second_protocol) {
	case '0':
	case 'A':
	case 'B':
		break;
	default:
		ret = E_ARG;
		goto out;
	}

	data[0] = first_protocol;
	data[1] = second_protocol;

	MUST(act_send(act, RF_CARD, RF_CARD_ACTIVATE, data, sizeof(data)));
	MUST(act_recv(act, r));

	if (!(atr && atr_length))
		goto out;

	if (r->code == RESPONSE_OK && *atr_length >= r->length - 6) {
		*atr_length = r->length - 6;
		memcpy(atr, r->data, *atr_length);
	} else if (r->code == RESPONSE_ERROR && *atr_length >= r->length - 5) {
		*atr_length = r->length - 5;
		memcpy(atr, r->data, *atr_length);
		ret = E_RESPONSE;
	} else {
		ret = E_ARG;
	}

out:
	return ret;
}

int act_rf_release(struct act *act, struct response *r)
{
	int ret;

	MUST(act_send(act, RF_CARD, RF_CARD_RELEASE, 0, 0));
	MUST(act_recv(act, r));

out:
	return ret;
}

int act_rf_status(struct act *act, struct response *r, unsigned short *card_type)
{
	int ret;

	MUST(act_send(act, RF_CARD, RF_CARD_STATUS, 0, 0));
	MUST(act_recv(act, r));

	if (!card_type)
		goto out;

	memcpy(card_type, r->data, sizeof(*card_type));

out:
	return ret;
}

int act_rf_mifare_verify_password(struct act *act, struct response *r, int use_key_a, unsigned char sector, unsigned char (*password)[6])
{
	unsigned char data[11];
	int ret;

	if (!(*password)) {
		ret = E_NULL;
		goto out;
	}

	if (sector > 0x0F) {
		ret = E_ARG;
		goto out;
	}

	data[0] = 0x00;
	data[1] = MIFARE_PASSWORD_VERIFICATION;
	data[2] = use_key_a > 0 ? 0x00 : 0x01;
	data[3] = sector;
	data[4] = sizeof(*password);
	memcpy(&data[5], *password, sizeof(*password));

	MUST(act_send(act, RF_CARD, RF_CARD_DATA_EXCHANGE_MIFARE, data, sizeof(data)));
	MUST(act_recv(act, r));

out:
	return ret;
}

int act_rf_mifare_load_password(struct act *act, struct response *r, int use_key_a, unsigned char sector)
{
	unsigned char data[4];
	int ret;

	if (sector > 0x0F) {
		ret = E_ARG;
		goto out;
	}

	data[0] = 0x00;
	data[1] = MIFARE_LOAD_CRYPTO_CHECKSUM_FROM_EEPROM;
	data[2] = use_key_a > 0 ? 0x00 : 0x01;
	data[3] = sector;

	MUST(act_send(act, RF_CARD, RF_CARD_DATA_EXCHANGE_MIFARE, data, sizeof(data)));
	MUST(act_recv(act, r));

out:
	return ret;
}

int act_rf_mifare_update_password(struct act *act, struct response *r, unsigned char sector, unsigned char (*password)[6])
{
	unsigned char data[11];
	int ret;

	if (!(*password)) {
		ret = E_NULL;
		goto out;
	}

	if (sector > 0x0F) {
		ret = E_ARG;
		goto out;
	}

	data[0] = 0x00;
	data[1] = MIFARE_MODIFY_SECTOR_PASSWORD_KEY_A;
	data[2] = 0x00;
	data[3] = sector;
	data[4] = sizeof(*password);
	memcpy(&data[5], *password, sizeof(*password));

	MUST(act_send(act, RF_CARD, RF_CARD_DATA_EXCHANGE_MIFARE, data, sizeof(data)));
	MUST(act_recv(act, r));

out:
	return ret;
}

int act_rf_mifare_download_password(struct act *act, struct response *r, int use_key_a, unsigned char sector, unsigned char (*password)[6])
{
	unsigned char data[11];
	int ret;

	if (!(*password)) {
		ret = E_NULL;
		goto out;
	}

	if (sector > 0x0F) {
		ret = E_ARG;
		goto out;
	}

	data[0] = 0x00;
	data[1] = MIFARE_DOWNLOAD_PASSWORD_TO_EEPROM;
	data[2] = use_key_a > 0 ? 0x00 : 0x01;
	data[3] = sector;
	data[4] = sizeof(*password);
	memcpy(&data[5], *password, sizeof(*password));

	MUST(act_send(act, RF_CARD, RF_CARD_DATA_EXCHANGE_MIFARE, data, sizeof(data)));
	MUST(act_recv(act, r));

out:
	return ret;
}

int act_rf_mifare_read_sector(struct act *act, struct response *r, unsigned char sector, unsigned char block_offset, unsigned char block_count, unsigned char *block_data, unsigned char block_size)
{
	unsigned char data[5];
	int ret;

	if (sector > 0x0F) {
		ret = E_ARG;
		goto out;
	}

	data[0] = 0x00;
	data[1] = MIFARE_READ_SECTOR_BLOCK_DATA;
	data[2] = sector;
	data[3] = block_offset;
	data[4] = block_count;

	MUST(act_send(act, RF_CARD, RF_CARD_DATA_EXCHANGE_MIFARE, data, sizeof(data)));
	MUST(act_recv(act, r));

	if (!block_data)
		goto out;

	if (r->code == RESPONSE_OK) {
		if (block_size > r->length - 6 - 2)
			block_size = r->length - 6 - 2;

		memcpy(block_data, r->data, block_size);
	}

out:
	return ret;
}

int act_rf_mifare_write_sector(struct act *act, struct response *r, unsigned char sector, unsigned char block_offset, unsigned char block_count, unsigned char *block_data, unsigned char block_size)
{
	unsigned char data[DATA_LEN];
	int ret;

	if (!block_data) {
		ret = E_NULL;
		goto out;
	}

	if (sector > 0x0F) {
		ret = E_ARG;
		goto out;
	}

	data[0] = 0x00;
	data[1] = MIFARE_WRITE_SECTOR_BLOCK_DATA;
	data[2] = sector;
	data[3] = block_offset;
	data[4] = block_count;
	memcpy(&data[5], block_data, block_size);

	MUST(act_send(act, RF_CARD, RF_CARD_DATA_EXCHANGE_MIFARE, data, block_size + 5));
	MUST(act_recv(act, r));

out:
	return ret;
}

int act_rf_mifare_initialize_value(struct act *act, struct response *r, unsigned char sector, unsigned char block, unsigned char (*value)[4])
{
	unsigned char data[DATA_LEN];
	int ret;

	if (!(*value)) {
		ret = E_NULL;
		goto out;
	}

	if (sector > 0x0F) {
		ret = E_ARG;
		goto out;
	}

	data[0] = 0x00;
	data[1] = MIFARE_INITIALIZE;
	data[2] = sector;
	data[3] = block;
	data[4] = sizeof(*value);
	memcpy(&data[5], *value, sizeof(*value));

	MUST(act_send(act, RF_CARD, RF_CARD_DATA_EXCHANGE_MIFARE, data, sizeof(*value) + 5));
	MUST(act_recv(act, r));

out:
	return ret;
}

int act_rf_mifare_read_value(struct act *act, struct response *r, unsigned char sector, unsigned char block, unsigned char (*value)[4])
{
	unsigned char data[DATA_LEN];
	int ret;

	if (!(*value)) {
		ret = E_NULL;
		goto out;
	}

	if (sector > 0x0F) {
		ret = E_ARG;
		goto out;
	}

	data[0] = 0x00;
	data[1] = MIFARE_READ;
	data[2] = sector;
	data[3] = block;

	MUST(act_send(act, RF_CARD, RF_CARD_DATA_EXCHANGE_MIFARE, data, 4));
	MUST(act_recv(act, r));

	if (!(*value))
		goto out;

	memcpy(*value, data, sizeof(*value));
out:
	return ret;
}

int act_rf_mifare_increment_value(struct act *act, struct response *r, unsigned char sector, unsigned char block, unsigned char (*value)[4])
{
	unsigned char data[DATA_LEN];
	int ret;

	if (!(*value)) {
		ret = E_NULL;
		goto out;
	}

	if (sector > 0x0F) {
		ret = E_ARG;
		goto out;
	}

	data[0] = 0x00;
	data[1] = MIFARE_INCREMENT;
	data[2] = sector;
	data[3] = block;
	data[4] = sizeof(*value);
	memcpy(&data[5], *value, sizeof(*value));

	MUST(act_send(act, RF_CARD, RF_CARD_DATA_EXCHANGE_MIFARE, data, sizeof(*value) + 5));
	MUST(act_recv(act, r));

out:
	return ret;
}

int act_rf_mifare_decrement_value(struct act *act, struct response *r, unsigned char sector, unsigned char block, unsigned char (*value)[4])
{
	unsigned char data[DATA_LEN];
	int ret;

	if (!(*value)) {
		ret = E_NULL;
		goto out;
	}

	if (sector > 0x0F) {
		ret = E_ARG;
		goto out;
	}

	data[0] = 0x00;
	data[1] = MIFARE_DECREMENT;
	data[2] = sector;
	data[3] = block;
	data[4] = sizeof(*value);
	memcpy(&data[5], *value, sizeof(*value));

	MUST(act_send(act, RF_CARD, RF_CARD_DATA_EXCHANGE_MIFARE, data, sizeof(*value) + 5));
	MUST(act_recv(act, r));

out:
	return ret;
}

int act_rf_wake_sleep(struct act *act, struct response *r)
{
	int ret;

	MUST(act_send(act, RF_CARD, RF_CARD_WAKE_SLEEP, 0, 0));
	MUST(act_recv(act, r));

out:
	return ret;
}

int act_card_serial_number(struct act *act, struct response *r, char *serial_number, int serial_number_length)
{
	int ret;

	MUST(act_send(act, CARD_SERIAL_NUMBER, CARD_SERIAL_NUMBER_READ, 0, 0));
	MUST(act_recv(act, r));

	if (!serial_number)
		goto out;

	if (r->code == RESPONSE_OK) {
		if (serial_number_length > r->length - 6)
			serial_number_length = r->length - 6;

		memcpy(serial_number, r->data, serial_number_length);
	}

out:
	return ret;
}

int act_reader_info(struct act *act, struct response *r, struct reader_info *info)
{
	int ret;

	MUST(act_send(act, READER_CONFIGURATION_INFO, READER_CONFIGURATION_INFO_READ, 0, 0));
	MUST(act_recv(act, r));

	if (!info)
		goto out;

	info->identity     = r->data[0];
	info->version[0]   = r->data[1];
	info->version[1]   = r->data[2];
	info->version[2]   = r->data[3];
	info->card_support = r->data[4];
	info->interface    = r->data[5];
	info->ic           = r->data[6];
	info->rf           = r->data[7];
	info->sam          = r->data[8];
	info->issue        = r->data[9];

out:
	return ret;
}

int act_version_info(struct act *act, struct response *r, int mode)
{
	int ret;

	switch (mode) {
	case VERSION_INFO_READ:
	case VERSION_INFO_READ_IC:
	case VERSION_INFO_READ_RF:
		break;
	default:
		ret = E_ARG;
		goto out;
	}

	MUST(act_send(act, VERSION_INFO, mode, 0, 0));
	MUST(act_recv(act, r));

out:
	return ret;
}

void act_print_message(struct response *r)
{
	char buf[BUF_LEN];

	if (r->code == RESPONSE_OK)
		fprintf(stdout, "%-20s%s\n", "ok:", act_ok_message(r, buf));
	else if (r->code == RESPONSE_ERROR)
		fprintf(stderr, "%-20s%s\n", "error:", act_error_message(r, buf));
	else
		fprintf(stderr, "%-20s%s\n", "error:", "response status is invalid");
}

const char * act_ok_message(struct response *r, char *buf)
{
	if (!r || r->code != RESPONSE_OK)
		return 0;

	switch (r->command) {
	case RESET_CARD:
		return act_reset_card_message(r, buf);

	case CHECK_STATUS:
		return act_check_status_message(r, buf);

	case MOVE_CARD:
		return act_move_card_message(r, buf);

	case SET_CARD:
		return act_set_card_message(r, buf);

	case DETECT_CARD:
		return act_detect_card_message(r, buf);

	case CPU_CARD:
		/* TODO */
		break;

	case SAM_CARD:
		/* TODO */
		break;

	case SLE_CARD:
		/* TODO */
		break;

	case IIC_CARD:
		/* TODO */
		break;

	case RF_CARD:
		return act_rf_card_message(r, buf);

	case CARD_SERIAL_NUMBER:
		return act_card_serial_number_message(r, buf);

	case READER_CONFIGURATION_INFO:
		return act_reader_configuration_info_message(r, buf);

	case VERSION_INFO:
		return act_version_info_message(r, buf);

	case RECOVERY_CARD_COUNT:
		/* TODO */
		break;
	
	}

	return "(no message)";
}

const char * act_error_message(struct response *r, char *buf)
{
	if (!r || r->code != RESPONSE_ERROR)
		return 0;

	if ( r->status.error == E_COMMAND_AT_END_OF_DEFINITION)
		return "command at end of definition";
	else if (r->status.error == E_COMMAND_PARAMETERS_WRONG)
		return "command parameters are wrong";
	else if (r->status.error == E_COMMAND_SEQUENCE_WRONG)
		return "command sequence is wrong";
	else if (r->status.error == E_COMMAND_NOT_SUPPORTED)
		return "command is not supported";
	else if (r->status.error == E_COMMAND_COMMUNICATION_ERROR)
		return "command communication error";
	else if (r->status.error == E_IC_CARD_RELEASE)
		return "ic card release";
	else if (r->status.error == E_CARD_BLOCKED)
		return "card blocked";
	else if (r->status.error == E_SENSOR_ERROR)
		return "sensor error";
	else if (r->status.error == E_LONG_CARD_ERROR)
		return "long card error";
	else if (r->status.error == E_SHORT_CARD_ERROR)
		return "short card error";
	else if (r->status.error == E_RECOVERY_CARD_PULLED_AWAY)
		return "recovery card pulled away";
	else if (r->status.error == E_IC_ELECTROMAGNET_ERROR)
		return "ic electromagnet error";
	else if (r->status.error == E_IC_CARD_MOVED)
		return "ic card moved";
	else if (r->status.error == E_CARD_ARTIFICIALLY_MOVED)
		return "ic card artificially moved";
	else if (r->status.error == E_COLLECTION_CARD_COUNTER_OVERFLOW)
		return "collection card counter overflow";
	else if (r->status.error == E_MOTOR_ERROR)
		return "motor error";
	else if (r->status.error == E_IC_CARD_POWER_SHORT_CIRCUIT)
		return "ic card power short circuit";
	else if (r->status.error == E_CARD_ACTIVATION_FAILED)
		return "card activation failed";
	else if (r->status.error == E_COMMAND_NOT_SUPPORTED_BY_IC_CARD)
		return "command not supported by ic card";
	else if (r->status.error == E_IC_CARD_DATA_TRANSMISSION_ERROR)
		return "ic card data transmission error";
	else if (r->status.error == E_IC_CARD_DATA_TRANSMISSION_TIMEOUT)
		return "ic card data transmission timeout";
	else if (r->status.error == E_CPU_SAM_CARD_NOT_MEET_STANDARD)
		return "cpu / sam card doesn't meet standard";
	else if (r->status.error == E_NO_CARD_STACK)
		return "no card stack";
	else if (r->status.error == E_COLLECTION_CARD_BOX_FULL)
		return "collection card box full";
	else if (r->status.error == E_END_OF_CARD_MACHINE_RESET)
		return "end of card machine reset";
	else
		return "warning: unknown error";
}

void act_set_read_response_interval(unsigned int millis)
{
	read_response_interval = millis;
}

const char * act_reset_card_message(struct response *r, char *buf)
{
	memcpy(buf, r->data, r->length - 6);
	buf[r->length - 6] = 0;
	return (char *) buf;
}

const char * act_check_status_message(struct response *r, char *buf)
{
	char tmp[64] = { 0 };
	unsigned short i;

	buf[0] = 0;

	if (r->parameters == CHECK_STATUS_BASIC || r->parameters == CHECK_STATUS_BASIC_SE) {
		switch (r->status.ok.st0) {
		case ST0_NO_CARD:
			strcat((char *) buf, "no card, ");
			break;

		case ST0_CARD_AT_SLOT:
			strcat((char *) buf, "card at slot, ");
			break;

		case ST0_RF_IC_CARD:
			strcat((char *) buf, "rf / ic card, ");
			break;
		}

		switch (r->status.ok.st1) {
		case ST1_CARD_ISSUER_NONE:
			strcat((char *) buf, "card issuer none, ");
			break;

		case ST1_CARD_ISSUER_FEW:
			strcat((char *) buf, "card issuer few, ");
			break;

		case ST1_CARD_ISSUER_FULL:
			strcat((char *) buf, "card issuer full, ");
			break;
		}

		switch (r->status.ok.st2) {
		case ST2_RECYCLING_BIN_UNDER:
			strcat((char *) buf, "recycling bin under, ");
			break;

		case ST2_RECYCLING_BIN_FULL:
			strcat((char *) buf, "recycling bin full, ");
			break;
		}
	}

	if (r->parameters == CHECK_STATUS_BASIC_SE) {
		for (i = 0; i < 10; i++) {
			if (i == 4)
				continue; /* system reserved */

			if (r->data[i] == 0x30)
				sprintf((char *) tmp, "%d| ", 0);
			else if (r->data[i] == 0x31)
				sprintf((char *) tmp, "%d| ", 1);

			strcat((char *) buf, tmp);
		}
	}

	return (char *) buf;
}

const char * act_move_card_message(struct response *r, char *buf)
{
	switch (r->parameters) {
	case MOVE_CARD_TO_EXIT:
		return "moved card to exit";
	case MOVE_CARD_TO_IC_SLOT:
		return "moved card to ic slot";
	case MOVE_CARD_TO_RF_SLOT:
		return "moved card to rf slot";
	case MOVE_CARD_TO_COLLECTION:
		return "moved card to collection";
	case MOVE_CARD_TO_OUTLET:
		return "moved card to outlet";
	default:
		return "warning: unknown move operation";
	}
}

const char * act_set_card_message(struct response *r, char *buf)
{
	switch (r->parameters) {
	case SET_CARD_PERMIT:
		return "permitting card insertion";

	case SET_CARD_DENY:
		return "denying card insertion";
	}

	return "(unknown parameters)";
}

const char * act_detect_card_message(struct response *r, char *buf)
{
	const unsigned short card_type = *((unsigned short *) r->data);

	switch (r->parameters) {
	case DETECT_CARD_AUTO_IC:
		switch (card_type) {
		case IC_UNKNOWN:
			return "unknown ic";
		case IC_T0_CPU:
			return "t=0 cpu";
		case IC_T1_CPU:
			return "t=1 cpu";
		case IC_SLE4442:
			return "sle4442";
		case IC_SLE4428:
			return "sle4428";
		case IC_AT24C01:
			return "at24c01";
		case IC_AT24C02:
			return "at24c02";
		case IC_AT24C04:
			return "at24c04";
		case IC_AT24C08:
			return "at24c08";
		case IC_AT24C16:
			return "at24c16";
		case IC_AT24C32:
			return "at24c32";
		case IC_AT24C64:
			return "at24c64";
		case IC_AT24C128:
			return "at24c128";
		case IC_AT24C256:
			return "at24c256";
		}
		break;

	case DETECT_CARD_AUTO_RF:
		switch (card_type) {
		case RF_UNKNOWN:
			return "unknown rf";
		case RF_MIFARE_ONE_S50:
			return "mifare one s50";
		case RF_MIFARE_ONE_S70:
			return "mifare one s70";
		case RF_MIFARE_ONE_UL:
			return "mifare one UL";
		case RF_TYPE_A_CPU:
			return "type-a cpu";
		case RF_TYPE_B_CPU:
			return "type-b cpu";
		}
		break;
	}

	return "(unknown card)";
}

const char * act_rf_card_message(struct response *r, char *buf)
{
	const unsigned char rtype = r->data[0];
	unsigned char pupi[4] = { 0 };
	unsigned char app_data[4] = { 0 };
	unsigned char protocol_info[3] = { 0 };
	unsigned char uid_len, i;
	unsigned short rf_type;
	char tmp[64] = { 0 };

	buf[0] = 0;

	switch (r->parameters) {
	case RF_CARD_ACTIVATE:
		switch (rtype) {
		case 'B':
			memcpy(pupi, r->data + 1, 4);
			memcpy(app_data, r->data + 5, 4);
			memcpy(app_data, r->data + 9, 3);

			strcat((char *) buf, "PUPI:");

			for (i = 0; i < 4; i++) {
				sprintf((char *) tmp, " %X", pupi[i]);
				strcat((char *) buf, tmp);
			}

			strcat((char *) buf, ". App Data:");

			for (i = 0; i < 4; i++) {
				sprintf((char *) tmp, " %X", app_data[i]);
				strcat((char *) buf, tmp);
			}

			strcat((char *) buf, ". Protocol Info: ");

			for (i = 0; i < 3; i++) {
				sprintf((char *) tmp, " %X", protocol_info[i]);
				strcat((char *) buf, tmp);
			}

			goto out;

		case 'M':
			/* Philips Mifare one card */
			if (memcmp(r->data + 1, ATQA_MIFARE_ULTRALIGHT, 2) == 0)
				strcat((char *) buf, "ATQA: Mifare Ultralight. UID:");
			else if (memcmp(r->data + 1, ATQA_MIFARE_S50_1K, 2) == 0)
				strcat((char *) buf, "ATQA: Mifare S50 1K. UID:");
			else if (memcmp(r->data + 1, ATQA_MIFARE_S70_4K, 2) == 0)
				strcat((char *) buf, "ATQA: Mifare S70 4K. UID:");
			else
				strcat((char *) buf, "(unknown ATQA), ");

			uid_len = r->data[3];
			for (i = 0; i < uid_len; i++) {
				sprintf((char *) tmp, " %X", r->data[i + 4]);
				strcat((char *) buf, tmp);
			}

			strcat((char *) buf, ". SAK: ");
			sprintf((char *) tmp, "%X", r->data[uid_len + 4]);
			strcat((char *) buf, tmp);

			if (rtype == 'M')
				goto out;

		case 'A':
			/* ISO/IEC 14443 Type A */
			goto out;

		default:
			return "(unknown rtype)";
		}

	case RF_CARD_RELEASE:
		return "card released";

	case RF_CARD_STATUS:
		rf_type = *((unsigned short *) r->data);

		switch (rf_type) {
		case RF_UNKNOWN:
			return "unknown rf";
		case RF_MIFARE_ONE_S50:
			return "mifare one s50";
		case RF_MIFARE_ONE_S70:
			return "mifare one s70";
		case RF_MIFARE_ONE_UL:
			return "mifare one UL";
		case RF_TYPE_A_CPU:
			return "type-a cpu";
		case RF_TYPE_B_CPU:
			return "type-b cpu";
		}

	case RF_CARD_DATA_EXCHANGE_MIFARE:
		if (memcmp(r->data + r->length - 6 - 2, RDATA_SUCCESS, 2) == 0)
			return "operation succeeded";
		else if (memcmp(r->data + r->length - 6 - 2, RDATA_FAILED, 2) == 0)
			return "operation failed";
		else if (memcmp(r->data + r->length - 6 - 2, RDATA_ADDRESS_OVERFLOW, 2) == 0)
			return "address overflow";
		else if (memcmp(r->data + r->length - 6 - 2, RDATA_LENGTH_OVERFLOW, 2) == 0)
			return "length overflow";
		else
			return "(unknown rdata)";

	case RF_CARD_DATA_EXCHANGE_TYPE_A:
	case RF_CARD_DATA_EXCHANGE_TYPE_B:
		break;

	default:
		return "(unknown parameters)";
	}

out:
	return (char *) buf;
}

const char * act_card_serial_number_message(struct response *r, char *buf)
{
	if (r->data[0] > 0)
		r->data[r->data[0] + 1] = 0;

	strcpy((char *) buf, (char *) &r->data[1]);
	return (char *) buf;
}

const char * act_reader_configuration_info_message(struct response *r, char *buf)
{
	buf[0] = 0;

	if (r->data[0] == ICRW_CONFIG_S1)
		strcat((char *) buf, "identity ok. ");
	else
		strcat((char *) buf, "identity mismatch. ");

	if (memcmp(r->data + 1, ICRW_CONFIG_S2_S4_CODE, 3) == 0)
		strcat((char *) buf, "standard version. ");
	else
		strcat((char *) buf, "custom version. ");

	switch (r->data[4]) {
	case ICRW_CONFIG_S5_NO_CARD_ISSUE:
		strcat((char *) buf, "no card issuing function. ");
		break;

	case ICRW_CONFIG_S5_IC_ONLY:
		strcat((char *) buf, "supports contactless IC card read/write only. ");
		break;

	case ICRW_CONFIG_S5_RF_ONLY:
		strcat((char *) buf, "supports RF card read/write only. ");
		break;

	case ICRW_CONFIG_S5_IC_RF:
		strcat((char *) buf, "supports IC and RF card read/write. ");
		break;
	}

	if (r->data[5] == ICRW_CONFIG_S6_INTERFACE_RS232)
		strcat((char *) buf, "using RS232 communication interface. ");
	else
		strcat((char *) buf, "using unknown communication interface. ");

	switch (r->data[6]) {
	case ICRW_CONFIG_S7_NO_IC_READ_WRITE:
		strcat((char *) buf, "no IC read/write functions. ");
		break;

	case ICRW_CONFIG_S7_IC_THIRD_PARTY_USE:
		strcat((char *) buf, "IC components for third-party use. ");
		break;

	case ICRW_CONFIG_S7_IC_STANDARD_READER:
		strcat((char *) buf, "standard IC reader. ");
		break;
	}

	switch (r->data[7]) {
	case ICRW_CONFIG_S8_NO_RF_READ_WRITE:
		strcat((char *) buf, "no RF read/write functions. ");
		break;

	case ICRW_CONFIG_S8_RF_THIRD_PARTY_USE:
		strcat((char *) buf, "RF components for third-party use. ");
		break;

	case ICRW_CONFIG_S8_RF_STANDARD_READER:
		strcat((char *) buf, "standard RF reader. ");
		break;
	}

	switch (r->data[8]) {
	case ICRW_CONFIG_S9_NO_SAM:
		strcat((char *) buf, "no SAM card function. ");
		break;

	case ICRW_CONFIG_S9_SAM1:
		strcat((char *) buf, "supports 1 SAM card. ");
		break;

	case ICRW_CONFIG_S9_SAM2:
		strcat((char *) buf, "supports 2 SAM cards. ");
		break;

	case ICRW_CONFIG_S9_SAM3:
		strcat((char *) buf, "supports 3 SAM cards. ");
		break;

	case ICRW_CONFIG_S9_SAM4:
		strcat((char *) buf, "supports 4 SAM cards. ");
		break;

	case ICRW_CONFIG_S9_SAM5:
		strcat((char *) buf, "supports 5 SAM cards. ");
		break;
	}

	switch (r->data[9]) {
	case ICRW_CONFIG_S10_TWIST:
		strcat((char *) buf, "twist card component issue.");
		break;

	case ICRW_CONFIG_S10_PULL:
		strcat((char *) buf, "pull card component issue.");
		break;
	}

	return (char *) buf;
}

const char * act_version_info_message(struct response *r, char *buf)
{
	r->data[r->length - 6] = 0;
	strcpy((char *) buf, (char *) r->data);
	return (char *) buf;
}
