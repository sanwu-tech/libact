#ifndef ACT_H
#define ACT_H

#if defined(__cplusplus)
extern "C" {
#endif

#define BUF_LEN (1024)
#define DATA_LEN (512)

#define DEFAULT_READ_RESPONSE_INTERVAL (100) // default read response interval in millis

/* Macro for switching values at compile-time based on endianness */
#if __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define BL(a, b) (a)
#else
#define BL(a, b) (b)
#endif

/* Error Codes (e1, e0) */
#define E_COMMAND_AT_END_OF_DEFINITION             BL(0x3030, 0x3030) /* 末定义的命令 */
#define E_COMMAND_PARAMETERS_WRONG                 BL(0x3130, 0x3031) /* 命令参数有错误 */
#define E_COMMAND_SEQUENCE_WRONG                   BL(0x3230, 0x3032) /* 命令执行顺序错误 */
#define E_COMMAND_NOT_SUPPORTED                    BL(0x3330, 0x3033) /* 硬件不支持命令 */
#define E_COMMAND_COMMUNICATION_ERROR              BL(0x3430, 0x3034) /* 命令数据错误(通讯包中 DATA 有错误) */
#define E_IC_CARD_RELEASE                          BL(0x3530, 0x3035) /* IC 卡接触末释放 */
#define E_CARD_BLOCKED                             BL(0x3031, 0x3130) /* 卡堵塞 */
#define E_SENSOR_ERROR                             BL(0x3231, 0x3132) /* 传感器错误 */
#define E_LONG_CARD_ERROR                          BL(0x3331, 0x3133) /* 长卡错误 */
#define E_SHORT_CARD_ERROR                         BL(0x3431, 0x3134) /* 短卡错误 */
#define E_RECOVERY_CARD_PULLED_AWAY                BL(0x3034, 0x3430) /* 回收卡时卡片被拔走 */
#define E_IC_ELECTROMAGNET_ERROR                   BL(0x3134, 0x3431) /* IC 卡电磁铁错误 */
#define E_IC_CARD_MOVED                            BL(0x3334, 0x3433) /* 卡片不能移动卡 IC 卡位 */
#define E_CARD_ARTIFICIALLY_MOVED                  BL(0x3434, 0x3434) /* 卡片被人为移动 */
#define E_COLLECTION_CARD_COUNTER_OVERFLOW         BL(0x3035, 0x3530) /* 收卡计数器溢出 */
#define E_MOTOR_ERROR                              BL(0x3135, 0x3531) /* 马达错误 */
#define E_IC_CARD_POWER_SHORT_CIRCUIT              BL(0x3036, 0x3630) /* IC卡供电电源短路 */
#define E_CARD_ACTIVATION_FAILED                   BL(0x3136, 0x3631) /* 卡激活失败 */
#define E_COMMAND_NOT_SUPPORTED_BY_IC_CARD         BL(0x3236, 0x3632) /* IC卡不支持当前命令 */
#define E_IC_CARD_AT_END_OF_ACTIVATION             BL(0x3536, 0x3635) /* IC 卡末激活 */
#define E_COMMAND_NOT_SUPPORTED_BY_CURRENT_IC_CARD BL(0x3636, 0x3636) /* 当前 IC 卡不支持命令 */
#define E_IC_CARD_DATA_TRANSMISSION_ERROR          BL(0x3736, 0x3637) /* 传输 IC 卡数据错误 */
#define E_IC_CARD_DATA_TRANSMISSION_TIMEOUT        BL(0x3836, 0x3638) /* 传输 IC 卡数据超时 */
#define E_CPU_SAM_CARD_NOT_MEET_STANDARD           BL(0x3936, 0x3639) /* CPU/SAM 卡不符合 EMV 标准 */
#define E_NO_CARD_STACK                            BL(0x3041, 0x4130) /* 发卡卡栈(箱)空,卡栈中无卡 */
#define E_COLLECTION_CARD_BOX_FULL                 BL(0x3141, 0x4131) /* 收卡箱满 */
#define E_END_OF_CARD_MACHINE_RESET                BL(0x3042, 0x4230) /* 卡机末复位 */

/* IC Card Types */
#define IC_UNKNOWN  BL(0x3030, 0x3030)
#define IC_T0_CPU   BL(0x3130, 0x3031)
#define IC_T1_CPU   BL(0x3131, 0x3131)
#define IC_SLE4442  BL(0x3230, 0x3032)
#define IC_SLE4428  BL(0x3231, 0x3132)
#define IC_AT24C01  BL(0x3330, 0x3033)
#define IC_AT24C02  BL(0x3331, 0x3133)
#define IC_AT24C04  BL(0x3332, 0x3233)
#define IC_AT24C08  BL(0x3333, 0x3333)
#define IC_AT24C16  BL(0x3334, 0x3433)
#define IC_AT24C32  BL(0x3335, 0x3533)
#define IC_AT24C64  BL(0x3336, 0x3633)
#define IC_AT24C128 BL(0x3337, 0x3733)
#define IC_AT24C256 BL(0x3338, 0x3833)

/* RF Card Types */
#define RF_UNKNOWN        BL(0x3030, 0x3030)
#define RF_MIFARE_ONE_S50 BL(0x3130, 0x3031)
#define RF_MIFARE_ONE_S70 BL(0x3131, 0x3131)
#define RF_MIFARE_ONE_UL  BL(0x3132, 0x3231)
#define RF_TYPE_A_CPU     BL(0x3230, 0x3032)
#define RF_TYPE_B_CPU     BL(0x3330, 0x3033)

/* ATQA */
extern const unsigned char ATQA_MIFARE_ULTRALIGHT[];
extern const unsigned char ATQA_MIFARE_S50_1K[];
extern const unsigned char ATQA_MIFARE_S70_4K[];

/* RDATA */
extern const unsigned char RDATA_SUCCESS[];
extern const unsigned char RDATA_FAILED[];
extern const unsigned char RDATA_ADDRESS_OVERFLOW[];
extern const unsigned char RDATA_LENGTH_OVERFLOW[];

/* Mifare */
#define MIFARE_PASSWORD_VERIFICATION            (0x20)
#define MIFARE_LOAD_CRYPTO_CHECKSUM_FROM_EEPROM (0x21)
#define MIFARE_MODIFY_SECTOR_PASSWORD_KEY_A     (0xD5)
#define MIFARE_DOWNLOAD_PASSWORD_TO_EEPROM      (0xD0)
#define MIFARE_READ_SECTOR_BLOCK_DATA           (0xB0)
#define MIFARE_WRITE_SECTOR_BLOCK_DATA          (0xD1)
#define MIFARE_INITIALIZE                       (0xD2)
#define MIFARE_READ                             (0xB1)
#define MIFARE_INCREMENT                        (0xD3)
#define MIFARE_DECREMENT                        (0xD4)

/* ICRW Config (S1 - S10) */
#define ICRW_CONFIG_S1                         ('7')
extern const unsigned char ICRW_CONFIG_S2_S4_CODE[];
#define ICRW_CONFIG_S5_NO_CARD_ISSUE           ('0')
#define ICRW_CONFIG_S5_IC_ONLY                 ('I')
#define ICRW_CONFIG_S5_RF_ONLY                 ('C')
#define ICRW_CONFIG_S5_IC_RF                   ('E')
#define ICRW_CONFIG_S6_INTERFACE_RS232         ('R')
#define ICRW_CONFIG_S7_NO_IC_READ_WRITE        ('0')
#define ICRW_CONFIG_S7_IC_THIRD_PARTY_USE      ('1')
#define ICRW_CONFIG_S7_IC_STANDARD_READER      ('2')
#define ICRW_CONFIG_S8_NO_RF_READ_WRITE        ('0')
#define ICRW_CONFIG_S8_RF_THIRD_PARTY_USE      ('1')
#define ICRW_CONFIG_S8_RF_STANDARD_READER      ('2')
#define ICRW_CONFIG_S9_NO_SAM                  ('0')
#define ICRW_CONFIG_S9_SAM1                    ('1')
#define ICRW_CONFIG_S9_SAM2                    ('2')
#define ICRW_CONFIG_S9_SAM3                    ('3')
#define ICRW_CONFIG_S9_SAM4                    ('4')
#define ICRW_CONFIG_S9_SAM5                    ('5')
#define ICRW_CONFIG_S10_TWIST                  ('0')
#define ICRW_CONFIG_S10_PULL                   ('1')

#define NUM_SENSORS (12)

/* Control Characters */
#define ACK (0x06)
#define NAK (0x15)
#define EOT (0x04)

#define STX  (0)
#define ADDR (1)
#define LENH (2)
#define LENL (3)
#define MT   (4)
#define CM   (5)
#define PM   (6)
#define ST0  (7)
#define ST1  (8)
#define ST2  (9)

#define STX_VALUE (0xF2)
#define CMT_VALUE (0x43)
#define EMT_VALUE (0x45)
#define PMT_VALUE (0x50)
#define ETX_VALUE (0x03)

#define RESPONSE_OK    ('P')
#define RESPONSE_ERROR ('N')

/* Internal Errors */
extern int E_ARG;
extern int E_FAIL;
extern int E_MEM;
extern int E_SUPP;
extern int E_IO;
extern int E_NULL;

/* Custom Errors */
extern int E_INVALID_ADDRESS;
extern int E_INVALID_BAUDRATE;
extern int E_INVALID_PORT;
extern int E_COMMAND_NOT_SENT;
extern int E_RESPONSE_NOT_RECEIVED;
extern int E_DATA_LENGTH_TOO_HIGH;
extern int E_RESPONSE_CHECKSUM_MISMATCH;
extern int E_RESPONSE;

struct act;

struct response_ok {
	unsigned char st0;
	unsigned char st1;
	unsigned char st2;
};

struct response_error {
	unsigned char e1;
	unsigned char e0;
};

union response_status {
	struct response_ok ok;
	unsigned short     error;
};

struct response {
	union response_status  status;
	unsigned short         length;
	unsigned char          code;
	unsigned char          address;
	unsigned char          command;
	unsigned char          parameters;
	unsigned char          data[DATA_LEN];
};

struct crs_status {
	unsigned char lane;
	unsigned char card_box;
	unsigned char capture_full;
};

struct reader_info {
	unsigned char identity;
	unsigned char version[3];
	unsigned char card_support;
	unsigned char interface;
	unsigned char ic;
	unsigned char rf;
	unsigned char sam;
	unsigned char issue;
};

extern int act_connect(struct act **act, const char *name, int baudrate, int address);
extern int act_disconnect(struct act *act);
extern int act_send(struct act *act, unsigned char command, unsigned char parameters, unsigned char *data, unsigned short data_length);
extern int act_recv(struct act *act, struct response *r);
extern int act_initialize(struct act *act, struct response *r, int mode, char *output, int output_length);
extern int act_get_crs_status(struct act *act, struct response *r, struct crs_status *status);
extern int act_get_sensors_status(struct act *act, struct response *r, unsigned char (*sensors)[NUM_SENSORS]);
extern int act_move_card(struct act *act, struct response *r, int mode);
extern int act_permit_insertion(struct act *act, struct response *r);
extern int act_deny_insertion(struct act *act, struct response *r);
extern int act_detect_ic(struct act *act, struct response *r, int *type);
extern int act_detect_rf(struct act *act, struct response *r, int *type);
extern int act_rf_activate(struct act *act, struct response *r, int first_protocol, int second_protocol, void *atr, int *atr_length);
extern int act_rf_release(struct act *act, struct response *r);
extern int act_rf_status(struct act *act, struct response *r, unsigned short *card_type);
extern int act_rf_mifare_verify_password(struct act *act, struct response *r, int use_key_a, unsigned char sector, unsigned char (*password)[6]);
extern int act_rf_mifare_load_password(struct act *act, struct response *r, int use_key_a, unsigned char sector);
extern int act_rf_mifare_update_password(struct act *act, struct response *r, unsigned char sector, unsigned char (*password)[6]);
extern int act_rf_mifare_download_password(struct act *act, struct response *r, int use_key_a, unsigned char sector, unsigned char (*password)[6]);
extern int act_rf_mifare_read_sector(struct act *act, struct response *r, unsigned char sector, unsigned char block_offset, unsigned char block_count, unsigned char *block_data, unsigned char block_size);
extern int act_rf_mifare_write_sector(struct act *act, struct response *r, unsigned char sector, unsigned char block_offset, unsigned char block_count, unsigned char *block_data, unsigned char block_size);
extern int act_rf_mifare_initialize_value(struct act *act, struct response *r, unsigned char sector, unsigned char block, unsigned char (*value)[4]);
extern int act_rf_mifare_read_value(struct act *act, struct response *r, unsigned char sector, unsigned char block, unsigned char (*value)[4]);
extern int act_rf_mifare_increment_value(struct act *act, struct response *r, unsigned char sector, unsigned char block, unsigned char (*value)[4]);
extern int act_rf_mifare_decrement_value(struct act *act, struct response *r, unsigned char sector, unsigned char block, unsigned char (*value)[4]);
extern int act_rf_wake_sleep(struct act *act, struct response *r);
extern int act_card_serial_number(struct act *act, struct response *r, char *serial_number, int serial_number_length);
extern int act_reader_info(struct act *act, struct response *r, struct reader_info *info);
extern int act_version_info(struct act *act, struct response *r, int mode);

extern void act_print_message(struct response *r);

extern const char * act_ok_message(struct response *r, char *buf);
extern const char * act_reset_card_message(struct response *r, char *buf);
extern const char * act_check_status_message(struct response *r, char *buf);
extern const char * act_move_card_message(struct response *r, char *buf);
extern const char * act_set_card_message(struct response *r, char *buf);
extern const char * act_detect_card_message(struct response *r, char *buf);
extern const char * act_rf_card_message(struct response *r, char *buf);
extern const char * act_card_serial_number_message(struct response *r, char *buf);
extern const char * act_reader_configuration_info_message(struct response *r, char *buf);
extern const char * act_version_info_message(struct response *r, char *buf);

extern const char * act_error_message(struct response *r, char *buf);

extern void act_set_read_response_interval(unsigned int millis);

/**
 * Reset Card
 * ==========
 * The instruction is the first instruction to be executed after power-on,
 * otherwise other instructions can not be executed, then the instruction can
 * be executed many times; When the instruction of the first performance, ICRW
 * automatic detection and determination HOST communication baud rate (BAUD),
 * according to the baud rate of communication; Once the command is executed
 * before will clear all error codes, the card into the card machine in the
 * disabled state, and returns the card software version information.
 */

#define RESET_CARD                         (0x30) /* 卡机复位 */
#define RESET_CARD_MOVE                    (0x30) /* 复位并将卡移动到卡口(持卡位) */
#define RESET_CARD_COLLECT                 (0x31) /* 复位并将卡回收到回收盒中 */
#define RESET_CARD_WITHOUT_MOVE            (0x33) /* 复位但不移动卡 */
#define RESET_CARD_MOVE_WITH_COUNT         (0x34) /* 同30H并启动回收卡计数功能 */
#define RESET_CARD_COLLECT_WITH_COUNT      (0x35) /* 同31H并启动回收卡计数功能 */
#define RESET_CARD_WITHOUT_MOVE_WITH_COUNT (0x37) /* 同33H 并启动回收卡计数功能 */

/**
 * Check Status
 * ============
 * Pm = 30H  Returns current card machine card machine card Have state st0, st1,
 * st2 (see specific meaning 7. Description)
 * Pm = 31H  Returns whether the current card machine card status and return
 * the card machine all the sensors (10 bytes) status information, as follows:
 */

#define CHECK_STATUS                       (0x31) /* 查状态 */
#define CHECK_STATUS_BASIC                 (0x30) /* 查询卡机当前基本状态 */
#define CHECK_STATUS_BASIC_SE              (0x31) /* 查询卡机当前基本状态(Se状态) */

/**
 * Move Card
 * =========
 * When the move card process can not move the card to the designated position,
 * the card will return the card jam error.
 * When the card is full recycling box, it will return "card collection box is
 * full error", and you should promptly clean up recycle bin card card.
 */

#define MOVE_CARD                          (0x32) /* 移动卡 */
#define MOVE_CARD_TO_EXIT                  (0x30) /* 将卡移动到出卡口(持卡位) */
#define MOVE_CARD_TO_IC_SLOT               (0x31) /* 将卡移动到IC卡位 */
#define MOVE_CARD_TO_RF_SLOT               (0x32) /* 将卡移动到RF卡位 */
#define MOVE_CARD_TO_COLLECTION            (0x33) /* 将卡回收到回收盒中 */
#define MOVE_CARD_TO_OUTLET                (0x39) /* 将卡移动到出卡口(不持卡位) */

/**
 * Set Card
 * ========
 * Setting a bayonet into the card to work to enable / disable. Enable a bayonet
 * into the card, a card is inserted through a card from the mouth, move the
 * card to the card machine card machine RF card operating position also
 * 
 * Available by the end of the investigation into the card status command.
 */

#define SET_CARD                           (0x33) /* 设置出卡口进卡 */
#define SET_CARD_PERMIT                    (0x30) /* 设置允许出卡口进卡 */
#define SET_CARD_DENY                      (0x31) /* 设置禁止出卡口进卡 */

/**
 * Detect Card
 * ===========
 * Automatic Test current type IC card, the card card card to go inside the IC
 * card, automatic test current IC card type.
 * Testing is complete return Card_type information.
 */

#define DETECT_CARD                        (0x50) /* IC卡/RF卡类型检测 */
#define DETECT_CARD_AUTO_IC                (0x30) /* 自动检测IC卡类型 */
#define DETECT_CARD_AUTO_RF                (0x31) /* 自动检测RF卡类型 */


/*
 * CPU Card
 * ========
 */

#define CPU_CARD                           (0x51) /* CPU卡操作 */

/**
 * CPU Card Cold Reset
 * -------------------
 * Card machine provides power (VCC), a clock signal (CLK), and a reset signal (RST)
 * to the card, the card is activated, and returns ATR.
 * Vcc = 30H to 5V power supply for the CPU card, EMV way CPU card reset (activate).
 * Vcc = 33H on the CPU card to 5V power supply, ISO7816 way CPU card reset (activate).
 * Vcc = 35H on the CPU card 3V power supply, ISO7816 way CPU card reset (activate).
 * Vcc is an optional parameter, if no command parameters Vcc, equivalent to Vcc = 30H.
 *
 * During reset, CPU card information does not comply with EMV ATR mode, returns e1, e0 = "69" error.
 * During reset, power failure detection IC card, return e1, e0 = "60" error.
 *
 * Type: CPU card protocol type
 * = 30H T = 0 protocol CPU Card
 * = 31H T = 1 protocol CPU Card
 * ATR: the answer to reset message format is as follows:
 * | TS | TO | [TA1] | [TB1] | ... | TCK |
 */

#define CPU_CARD_COLD_RESET                (0x30) /* CPU卡冷复位 */

/**
 * CPU Card Power Off
 * ------------------
 * Electrically operated under the CPU card.
 * It has the power to activate the next CPU card electrically operated.
 */

#define CPU_CARD_POWER_OFF                 (0x31) /* CPU卡下电 */

/**
 * CPU Card Inquiry
 * ----------------
 * Check the status of the current operation of the CPU card, return Sti status:
 * Sti = 30H end card activation
 *     = 31H card has been activated, the current CPU card operation clock frequency of 3.57 MHz
 *     = 32H card has been activated, the current CPU card operation clock frequency of 7.16 MHz
 * Check the power supply to the IC card circuit fails, it returns e1, e0 = "60" error.
 */

#define CPU_CARD_INQUIRY                   (0x32) /* CPU卡状态查询 */

/**
 * CPU Card Data Exchange
 * ----------------------
 * Have been reset (activate) the success of the CPU card T = 0 protocol for data exchange operations.
 *
 * C-APDU HOST sent to T = 0 CPU card data packet, a minimum of 4 byte, a maximum of 261 byte. Format is as follows:
 * CLA | INS | P1 | P2 | [LC] | [DATA1] | ... | Le  
 *
 * R-APDU CPU card is returned to the HOST data packet, a minimum of 2 byte, a maximum of 258 byte format is as follows:
 * [Data1] | ... | [Data(n)] | Sw1 | Sw0 |
 *
 * The detection during the operation of the IC card power supply fails, the card machine returns e1, e0 - = "60" error;
 * The protocol is not the current CPU card T = 0 protocol, the card machine returns an error code e1, e0 = "62" error; such as CPU card
 * Timeout before the next card machine card CPU power, then return e1, e0 = "63" error; other protocols such as errors,
 * Card card machine to the next CPU power, and then returns an error code e1, e0 = "64" error; as HOST CPU in the end executed
 * Row reset activation, the card machine returns e1, e0 = "65" error
 */

#define CPU_CARD_DATA_EXCHANGE_T0          (0x33) /* T=0协议CPU卡APDU数据交换 */
#define CPU_CARD_DATA_EXCHANGE_T1          (0x34) /* T=1协议CPU卡APDU数据交换 */

/**
 * CPU Card Warm Reset
 * -------------------
 * The command is in the IC card is activated, the card machine to do the reset
 * operation, regain ATR.
 * 
 * Type: CPU card protocol type
 * = 30H T = 0 protocol CPU Card
 * = 31H T = 1 protocol CPU Card
 */

#define CPU_CARD_WARM_RESET                (0x38) /* CPU卡热复位 */

/**
 * CPU Card Data Exchange Auto
 * ---------------------------
 * Card automatically determine the current CPU card T = 0 / T = 1 protocol, the
 * protocol automatically selects the C-APDU operation returns R-APDU. Detected
 * during operation of the IC card power supply fails, the card machine returns
 * e1, e0 - = "60" error;
 * 
 * The protocol is not the current CPU card T = 0 / T = 1 protocol, the card
 * machine returns an error code e1, e0 = "62" error; such as CPU card expires,
 * the card machine card before the next CPU power, then return e1, e0 = "63"
 * error; other protocols, such as error Appears first on the next card machine
 * card CPU power, and then returns an error code e1, e0 = "64" error; as in HOST
 * CPU at the end of the reset operation activated, the card machine returns e1,
 * e0 = "65" error
 */

#define CPU_CARD_DATA_EXCHANGE_AUTO        (0x39) /* 自动区分T=0/T=1协议CPU卡APDU数据交换 */


#define SAM_CARD                           (0x52) /* SAM卡操作 */
#define SAM_CARD_COLD_RESET                (0x30) /* SAM卡冷复位 */
#define SAM_CARD_POWER_OFF                 (0x31) /* SAM卡下电 */
#define SAM_CARD_INQUIRE                   (0x32) /* SAM卡状态查询 */
#define SAM_CARD_DATA_EXCHANGE_T0          (0x33) /* T=0协议SAM卡APDU数据交换 */
#define SAM_CARD_DATA_EXCHANGE_T1          (0x33) /* T=1协议SAM卡APDU数据交换 */
#define SAM_CARD_WARM_RESET                (0x38) /* SAM卡热复位 */
#define SAM_CARD_DATA_EXCHANGE_AUTO        (0x39) /* 自动区分T=0/T=1协议SAM卡APDU数据交换 */
#define SAM_CARD_SELECT                    (0x40) /* 选择SAM卡座 */


#define SLE_CARD                           (0x53) /* SLE4442/4428卡操作 */
#define SLE_CARD_ACTIVATE                  (0x30) /* SLE4442/4428卡复位(激活) */
#define SLE_CARD_RELEASE                   (0x31) /* SLE4442/4428卡下电(释放) */
#define SLE_CARD_STATUS                    (0x32) /* 查SLE4442/4428卡状态 */
#define SLE_CARD_OPERATE_4442              (0x33) /* 操作SLE4442卡 */
#define SLE_CARD_OPERATE_4428              (0x33) /* 操作SLE4428卡 */


#define IIC_CARD                           (0x54) /* IIC 存贮卡操作 */
#define IIC_CARD_ACTIVATE                  (0x30) /* IIC卡复位(激活) */
#define IIC_CARD_RELEASE                   (0x31) /* IIC卡下电(释放) */
#define IIC_CARD_STATUS                    (0x32) /* 查IIC卡状态 */
#define IIC_CARD_READ                      (0x33) /* 读IIC卡 */
#define IIC_CARD_WRITE                     (0x33) /* 写IIC卡 */

#define RF_CARD                            (0x60) /* RF卡操作(13.56 MHZ) */
#define RF_CARD_ACTIVATE                   (0x30) /* RF卡激活 */
#define RF_CARD_RELEASE                    (0x31) /* RF卡下电释放 */
#define RF_CARD_STATUS                     (0x32) /* RF卡操作状态查询 */

/**
 * RF Card Data Exchange Mifare
 * ---------------
 * Against Mifare 1 card (read and write) operations, command data used is similar
 * to that by ISO / IEC 7816 T = 0 standard data exchange command (C-APDU) form
 * feed line operation. Therefore, card machine received a command to specify the
 * meaning of the data on the card and then execute the appropriate action. When
 * the command is executed successfully, 9000H increase in the return to normal
 * packet;
 *
 * Error executing command, similar to the normal return returned only ISO / IEC
 * 7816-3 T = 0 in the standard specification "sw1 + sw2" two error response code.
 */

#define RF_CARD_DATA_EXCHANGE_MIFARE       (0x33) /* Mifare 标准卡读写 */

/**
 * RF Card Data Exchange Type A
 * ----------------------------
 * This command is based on the ISO/IEC 14443-4 specifications, implementation
 * of RF card of Type A T=CL agreement for data exchange operations.
 *
 * Note:
 * C-APDU maximum length of 261 byte; R-APDU maximum length of 258 byte.
 */

#define RF_CARD_DATA_EXCHANGE_TYPE_A       (0x34) /* Type A 标准T=CL卡APDU数据交换 */

/**
 * RF Card Data Exchange Type B
 * ----------------------------
 * This command is based on the ISO/IEC 14443-4 specifications, implementation
 * of RF card of Type B T=CL agreement for data exchange operations.
 *
 * Note:
 * C-APDU maximum length of 261 byte; R-APDU maximum length of 258 byte.
 */

#define RF_CARD_DATA_EXCHANGE_TYPE_B       (0x35) /* Type B 标准T=CL卡APDU数据交换 */

/**
 * RF Card Sleep / Wake
 * --------------------
 * Set = 30H Sleep operation of RF card
 * Set = 31H of the RF card wakeup
 */

#define RF_CARD_WAKE_SLEEP                 (0x39) /* RF 卡唤醒/睡眠 */


#define CARD_SERIAL_NUMBER                 (0xA2) /* 卡机序列号 */

/**
 * Card Serial Number Read
 * -----------------------
 * len: read card machine serial number data length (the minimum is 0 bytes and a maximum of 18 bytes)
 * ICRW_SN: Card Serial Number
 */

#define CARD_SERIAL_NUMBER_READ            (0x30) /* 读卡机序列号 */


#define READER_CONFIGURATION_INFO          (0xA3) /* 读卡机配置信息 */
#define READER_CONFIGURATION_INFO_READ     (0x30) /* 读取卡机机型配置信息 */


#define VERSION_INFO                       (0xA4) /* 版本信息读取 */
#define VERSION_INFO_READ                  (0x30) /* 卡机软件版本信息 */
#define VERSION_INFO_READ_IC               (0x31) /* IC卡机软件版本信息 */
#define VERSION_INFO_READ_RF               (0x32) /* IC卡机软件版本信息 */


#define RECOVERY_CARD_COUNT                (0xA5) /* 回收卡计数 */
#define RECOVERY_CARD_COUNT_READ           (0x30) /* 读取回收卡计数的数值 */
#define RECOVERY_CARD_COUNT_SET_INITIAL    (0x31) /* 设置回收卡计数初始值 */


/* Card Status Code */
#define ST0_NO_CARD                        ('0') /* 卡机通道内无卡 */
#define ST0_CARD_AT_SLOT                   ('1') /* 卡机通道出卡口处有一张卡 */
#define ST0_RF_IC_CARD                     ('2') /* 卡机通道 RF/IC 卡位有卡 */

#define ST1_CARD_ISSUER_NONE               ('0') /* 发卡箱无卡 */
#define ST1_CARD_ISSUER_FEW                ('1') /* 发卡箱卡少 */
#define ST1_CARD_ISSUER_FULL               ('2') /* 发卡箱卡足 */

#define ST2_RECYCLING_BIN_UNDER            ('0') /* 回收箱未满 */
#define ST2_RECYCLING_BIN_FULL             ('1') /* 回收箱卡满 */

#if defined(__cplusplus)
}
#endif

#endif
