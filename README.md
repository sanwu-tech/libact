# libact
libact is a library for controlling ACT card dispensers.

## Build
1. Install [libserialport >= 0.1.1](https://sigrok.org/wiki/Downloads#Binaries_and_distribution_packages)
2. Run `cmake .`, `make`, then `make install`

## License
This project is GPL-3.0 licensed.
